public class Universidad {
    //ATRIBUTOS
    private String nombre;
    private String direccion;
    private String nit;
    private String telefono;
    private String email;
    private boolean publica;

    //CONSTRUCTOR
    public Universidad(String nombre, String direccion, String nit, String telefono, boolean publica){
        //this.nombre -> Atributo
        //nombre -> Parámetro
        this.nombre = nombre;
        this.direccion = direccion;
        this.nit = nit;
        this.telefono = telefono;
        this.publica = publica;
    }

    /***********************
     * MÉTODOS CONSULTORES
     **********************/
    public String getNombre() {
        return nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public String getNit() {
        return nit;
    }

    public String getTelefono() {
        return telefono;
    }

    public String getEmail() {
        return email;
    }

    public boolean isPublica() {
        return publica;
    }

     /***********************
     * MÉTODOS MODIFICADORES
     **********************/
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    

    /************
    * ACCIONES
    ***********/
    public void matricular_estudiante(){

    }

    public void consultar_estudiante(){

    }

    public void contratar_docente(){

    }

    public void crear_facultad(){

    }

    
}
