public class Facultad {
    //ATRIBUTOS
    private String nombre;
    private String codigo;

    //CONSTRUCTOR
    public Facultad(String nombre, String codigo){
        this.nombre = nombre;
        this.codigo = codigo;
    }

    //CONSULTORES

    public String getNombre() {
        return nombre;
    }

    public String getCodigo() {
        return codigo;
    }
    //MODIFICADORES

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
